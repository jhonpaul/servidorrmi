
package RMI;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.List;


public interface interfProducto extends Remote{
    public boolean guardar(Producto producto) throws RemoteException;
    public boolean actualizar(Producto producto) throws RemoteException;
    public boolean eliminar(int id) throws RemoteException;
    public List<Producto> buscarTodos() throws RemoteException;
    public Producto buscarProducto( int id) throws RemoteException;
    
}
