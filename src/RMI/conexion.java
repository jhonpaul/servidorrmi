
package RMI;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import javax.swing.JOptionPane;


public class conexion {
    
//    private static String db="basereserva";
//    private static String url="jdbc:mysql://127.0.0.1/"+db;
//    private static String user="root";
//    private static String pass="";
//    private static Connection link=null;
       
    private  Connection link=null;
    
    
    public conexion(String db,String host,String user,String pass){
        
        
        String url="jdbc:mysql://"+host+":3306/"+db;
        try{
            Class.forName("com.mysql.jdbc.Driver");
            link=DriverManager.getConnection(url,user,pass);
        }catch(ClassNotFoundException | SQLException e){
            JOptionPane.showConfirmDialog(null, e);
        }
        
    }

    public  Connection getLink() {
        return link;
    }

    public  void setLink(Connection link) {
        this.link = link;
    }
    
    
}
