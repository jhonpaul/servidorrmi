
package RMI;


import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JOptionPane;


public class productoImpl extends UnicastRemoteObject implements interfProducto{
    
    private Connection con=null;
    private PreparedStatement ps = null;
    private ResultSet rs=null;
    private String sSQL = "";
    
    private String host,user,pass;
    
    public productoImpl(String host,String user, String pas) throws RemoteException{
       this.host=host;
        this.user=user;
        this.pass=pas;
    }

    @Override
    public boolean guardar(Producto producto) throws RemoteException {
        
         sSQL = "insert into producto (nombre,descripcion,unidad_medida,precio_venta)"
                + "values (?,?,?,?)";
        try {
            conexion conn = new conexion("basereserva",host,user,pass);
            con = conn.getLink();
            PreparedStatement pst = con.prepareStatement(sSQL);
            pst.setString(1, producto.getNombre());
            pst.setString(2, producto.getDescripcion());
            pst.setString(3, producto.getUnidad_medida());
            pst.setDouble(4, producto.getPrecio_venta());


            int n = pst.executeUpdate();

            if (n != 0) {
                return true;
            } else {
                return false;
            }

        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    @Override
    public boolean actualizar(Producto producto) throws RemoteException {
       sSQL = "update producto set nombre=? ,descripcion=?,unidad_medida=?,precio_venta=?"
                + "where idproducto=?";


        try {
            PreparedStatement pst = con.prepareStatement(sSQL);
            pst.setString(1, producto.getNombre());
            pst.setString(2, producto.getDescripcion());
            pst.setString(3, producto.getUnidad_medida());
            pst.setDouble(4, producto.getPrecio_venta());

            pst.setInt(5, producto.getIdproducto());
            int n = pst.executeUpdate();

            if (n != 0) {
                return true;
            } else {
                return false;
            }



        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    @Override
    public boolean eliminar(int id) throws RemoteException {
          sSQL = "delete from producto where idproducto=?";

        try {
            PreparedStatement pst = con.prepareStatement(sSQL);
            pst.setInt(1, id);
            int n = pst.executeUpdate();


            if (n != 0) {
                return true;
            } else {
                return false;
            }

        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    @Override
    public List<Producto> buscarTodos() throws RemoteException {
         sSQL = "select * from producto ";

        try {
            Producto p=null;
            Statement st = con.createStatement();
            ResultSet rs = st.executeQuery(sSQL);
            List<Producto> Productos= new ArrayList<Producto>();
            while (rs.next()) {
                p= new Producto();
                p.setIdproducto(rs.getInt("idproducto"));
                p.setNombre(rs.getString("nombre"));
                p.setDescripcion(rs.getString("descripcion"));
                p.setUnidad_medida(rs.getString("unidad_medida"));
                p.setPrecio_venta(rs.getDouble("precio_venta"));
                Productos.add(p);
            }
           return Productos;

        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public Producto buscarProducto(int id) throws RemoteException {
         sSQL = "select * from producto where idproducto=?";
        Producto p=null;
        try {
            
            PreparedStatement pst = con.prepareStatement(sSQL);
            pst.setInt(1, id);
                    
            ResultSet rs = pst.executeQuery(sSQL);
            
           
            while (rs.next()) {
                p= new Producto();
                p.setIdproducto(rs.getInt("idproducto"));
                p.setNombre(rs.getString("nombre"));
                p.setDescripcion(rs.getString("descripcion"));
                p.setUnidad_medida(rs.getString("unidad_medida"));
                p.setPrecio_venta(rs.getDouble("precio_venta"));
                
            }
           return p;

        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
    
    
}