package RmiHotel;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;


public class clienteImpl extends UnicastRemoteObject implements interfCliente {

    private Connection con = null;
    private PreparedStatement ps = null;
    private ResultSet rs = null;
    private String sSQL = "";
    private String host, user, pass;

    public clienteImpl(String host, String user, String pas) throws RemoteException {
        this.host = host;
        this.user = user;
        this.pass = pas;
    }

    @Override
    public boolean guardar(Cliente cliente) throws RemoteException {

        sSQL = "insert into cliente (nombre,apaterno,amaterno,tipo_documento,numero_documento,direccion,telefono,email)"
                + "values (?,?,?,?,?,?,?,?)";
        try {
            RMI.conexion conn = new RMI.conexion("reservahotel", host, user, pass);
            con = conn.getLink();
            PreparedStatement pst = con.prepareStatement(sSQL);
            pst.setString(1, cliente.getNombre());
            pst.setString(2, cliente.getApaterno());
            pst.setString(3, cliente.getAmaterno());
            pst.setString(4, cliente.getTipo_documento());
            pst.setString(5, cliente.getNumero_documento());
            pst.setString(6, cliente.getDireccion());
            pst.setString(7, cliente.getTelefono());
            pst.setString(8, cliente.getEmail());


            int n = pst.executeUpdate();

            if (n != 0) {
                return true;
            } else {
                return false;
            }
           
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    @Override
    public boolean actualizar(Cliente cliente) throws RemoteException {
        sSQL = "update cliente set nombre=? ,apaterno=?,amaterno=?,tipo_documento=?,direccion=?,telefono=?,email=?"
                + "where numero_documento=?";


        try {
             RMI.conexion conn = new RMI.conexion("reservahotel", host, user, pass);
            con = conn.getLink();
            PreparedStatement pst = con.prepareStatement(sSQL);
            pst.setString(1, cliente.getNombre());
            pst.setString(2, cliente.getApaterno());
            pst.setString(3, cliente.getAmaterno());
            pst.setString(4, cliente.getTipo_documento());
            pst.setString(5, cliente.getDireccion());
            pst.setString(6, cliente.getTelefono());
            pst.setString(7, cliente.getEmail());
            
            pst.setString(8,cliente.getNumero_documento());
            int n = pst.executeUpdate();

            if (n != 0) {
                return true;
            } else {
                return false;
            }



        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    @Override
    public boolean eliminar(int id) throws RemoteException {
        sSQL = "delete from cliente where idcliente=?";

        try {
            RMI.conexion conn = new RMI.conexion("reservahotel", host, user, pass);
            con = conn.getLink();
            PreparedStatement pst = con.prepareStatement(sSQL);
            pst.setInt(1, id);
            int n = pst.executeUpdate();


            if (n != 0) {
                return true;
            } else {
                return false;
            }

        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    @Override
    public  List<Cliente> buscarTodos() throws RemoteException {
        sSQL = "select * from cliente ";

        try {
             RMI.conexion conn = new RMI.conexion("reservahotel", host, user, pass);
            con = conn.getLink();
            Cliente c = null;
            Statement st = con.createStatement();
            ResultSet rs = st.executeQuery(sSQL);
            List<Cliente> Clientes = new ArrayList<Cliente>();
            while (rs.next()) {
                c = new Cliente();
                c.setIdcliente(rs.getInt("idcliente"));
                c.setNombre(rs.getString("nombre"));
                c.setApaterno(rs.getString("apaterno"));
                c.setAmaterno(rs.getString("amaterno"));
                c.setTipo_documento(rs.getString("tipo_documento"));
                c.setNumero_documento(rs.getString("numero_documento"));
                c.setDireccion(rs.getString("direccion"));
                c.setTelefono(rs.getString("telefono"));
                c.setEmail(rs.getString("email"));
                Clientes.add(c);
            }
            return Clientes;

        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public Cliente buscarCliente(String numDoc) throws RemoteException {
        int totalregistros = 0;
        

        sSQL = "select idcliente,nombre, apaterno, amaterno, tipo_documento, numero_documento, direccion, telefono,email from cliente"
                + " where numero_documento=?";

        Cliente c = null;
        try {
            RMI.conexion conn = new RMI.conexion("reservahotel", host, user, pass);
            con = conn.getLink();
            PreparedStatement pst = con.prepareStatement(sSQL);
        
            pst.setString(1,numDoc);
            
            ResultSet rs = pst.executeQuery();
            

            while (rs.next()) {
                c = new Cliente();
               c.setIdcliente(rs.getInt("idcliente"));
                c.setNombre(rs.getString("nombre"));
                c.setApaterno(rs.getString("apaterno"));
                c.setAmaterno(rs.getString("amaterno"));
                c.setTipo_documento(rs.getString("tipo_documento"));
                c.setNumero_documento(rs.getString("numero_documento"));
                c.setDireccion(rs.getString("direccion"));
                c.setTelefono(rs.getString("telefono"));
                c.setEmail(rs.getString("email"));
            }
            return c;



        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

   

  
}