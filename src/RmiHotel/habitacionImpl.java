
package RmiHotel;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;



public class habitacionImpl extends UnicastRemoteObject implements interfHabitacion{
    
    private Connection con=null;
    private PreparedStatement ps = null;
    private ResultSet rs=null;
    private String sSQL = "";
    
    private String host,user,pass;
    
    public habitacionImpl(String host,String user, String pas) throws RemoteException{
       this.host=host;
        this.user=user;
        this.pass=pas;
    }

     @Override
    public boolean guardar(Habitacion habitacion) throws RemoteException {
        
         sSQL = "insert into habitacion (numero,piso,descripcion,caracteristicas,precio_diario,estado,tipo_habitacion)"
                + "values (?,?,?,?,?,?,?)";
        try {
            RMI.conexion conn = new RMI.conexion("reservahotel",host,user,pass);
            con = conn.getLink();
            PreparedStatement pst = con.prepareStatement(sSQL);
            pst.setString(1, habitacion.getNumero());
            pst.setString(2, habitacion.getPiso());
            pst.setString(3, habitacion.getDescripcion());
            pst.setString(4, habitacion.getCaracteristicas());
            pst.setDouble(5, habitacion.getPrecio_diario());
            pst.setString(6, habitacion.getEstado());
            pst.setString(7, habitacion.getTipo_habitacion());
           


            int n = pst.executeUpdate();

            if (n != 0) {
                return true;
            } else {
                return false;
            }

        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    @Override
    public boolean actualizar(Habitacion habitacion) throws RemoteException {
       sSQL = "update habitacion set numero=? ,piso=?,descripcion=?,caracteristicas=?,precio_diario=?,estado=?,tipo_habitacion=?"
                + "where idhabitacion=?";


        try {
            RMI.conexion conn = new RMI.conexion("reservahotel",host,user,pass);
            con = conn.getLink();
            PreparedStatement pst = con.prepareStatement(sSQL);
            pst.setString(1, habitacion.getNumero());
            pst.setString(2, habitacion.getPiso());
            pst.setString(3, habitacion.getDescripcion());
            pst.setString(4, habitacion.getCaracteristicas());
            pst.setDouble(5, habitacion.getPrecio_diario());
            pst.setString(6, habitacion.getEstado());
            pst.setString(7, habitacion.getTipo_habitacion());

            
            pst.setInt(9, habitacion.getIdHabitacion());
            int n = pst.executeUpdate();

            if (n != 0) {
                return true;
            } else {
                return false;
            }



        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

     @Override
    public boolean eliminar(int id) throws RemoteException {
          sSQL = "delete from habitacion where idhabitacion=?";

        try {
            RMI.conexion conn = new RMI.conexion("reservahotel",host,user,pass);
            con = conn.getLink();
            PreparedStatement pst = con.prepareStatement(sSQL);
            pst.setInt(1, id);
            int n = pst.executeUpdate();


            if (n != 0) {
                return true;
            } else {
                return false;
            }

        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

@Override
    public List<Habitacion> buscarTodos() throws RemoteException {
         sSQL = "select * from habitacion";

        try {
            RMI.conexion conn = new RMI.conexion("reservahotel",host,user,pass);
            con = conn.getLink();
            Habitacion h=null;
            Statement st = con.createStatement();
            ResultSet rs = st.executeQuery(sSQL);
            List<Habitacion> habitaciones= new ArrayList<Habitacion>();
            while (rs.next()) {
                h= new Habitacion();
                h.setIdHabitacion(rs.getInt("idhabitacion"));
                h.setNumero(rs.getString("numero"));
                h.setPiso(rs.getString("piso"));
                h.setDescripcion(rs.getString("descripcion"));
                h.setCaracteristicas(rs.getString("caracteristicas"));
                h.setPrecio_diario(rs.getDouble("precio_diario"));
                h.setEstado(rs.getString("estado"));
                h.setTipo_habitacion(rs.getString("tipo_habitacion"));
               
                habitaciones.add(h);
            }
           return habitaciones;

        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

 
    public Habitacion buscarHabitacion(int id) throws RemoteException {
         sSQL = "select * from habitacion where idhabitacion=?";
        Habitacion h=null;
        try {
            RMI.conexion conn = new RMI.conexion("reservahotel",host,user,pass);
            con = conn.getLink();
            PreparedStatement pst = con.prepareStatement(sSQL);
            pst.setInt(1, id);
                    
            ResultSet rs = pst.executeQuery(sSQL);
            
           
            while (rs.next()) {
              h= new Habitacion();
                h.setIdHabitacion(rs.getInt("idhabitacion"));
                h.setNumero(rs.getString("numero"));
                h.setPiso(rs.getString("piso"));
                h.setDescripcion(rs.getString("descripcion"));
                h.setCaracteristicas(rs.getString("caracteristicas"));
                h.setPrecio_diario(rs.getDouble("precio_diario"));
                h.setEstado(rs.getString("estado"));
                h.setTipo_habitacion(rs.getString("tipo_habitacion"));
                
            }
           return h;

        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

  

   
    
    
}