package RmiHotel;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class reservaImpl extends UnicastRemoteObject implements interfReserva {

    private Connection con = null;
    private PreparedStatement ps = null;
    private ResultSet rs = null;
    private String sSQL = "";
    private String host, user, pass;

    public reservaImpl(String host, String user, String pas) throws RemoteException {
        this.host = host;
        this.user = user;
        this.pass = pas;
    }

    @Override
    public boolean guardar(Reserva reserva) throws RemoteException {

        sSQL = "insert into reserva (tipo_reserva,fecha_reserva,fecha_ingreso,fecha_salida,costo,idhabitacion,idcliente)"
                + "values (?,?,?,?,?,?,?,?,?)";
        try {
            RMI.conexion conn = new RMI.conexion("reservaHotel", host, user, pass);
            con = conn.getLink();
            PreparedStatement pst = con.prepareStatement(sSQL);
            pst.setString(1, reserva.getTipo_reserva());
            pst.setDate(2, reserva.getFecha_reserva());
            pst.setDate(3, reserva.getFecha_ingreso());
            pst.setDate(4, reserva.getFecha_salida());
            pst.setDouble(5, reserva.getCosto());
            pst.setInt(6, reserva.getHabitacion().getIdHabitacion());
            pst.setInt(7, reserva.getCliente().getIdcliente());



            int n = pst.executeUpdate();

            if (n != 0) {
                return true;
            } else {
                return false;
            }

        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    @Override
    public boolean actualizar(Reserva reserva) throws RemoteException {
        sSQL = "update reserva set tipo_reserva=?,fecha_reserva=?,fecha_ingreso=?,fecha_salida=?,costo=?,idhabitacion=?,idcliente=?"
                + "where idcliente=?";


        try {
            RMI.conexion conn = new RMI.conexion("reservaHotel", host, user, pass);
            con = conn.getLink();
            PreparedStatement pst = con.prepareStatement(sSQL);
            pst.setString(1, reserva.getTipo_reserva());
            pst.setDate(2, reserva.getFecha_reserva());
            pst.setDate(3, reserva.getFecha_ingreso());
            pst.setDate(4, reserva.getFecha_salida());
            pst.setDouble(5, reserva.getCosto());
            pst.setInt(6, reserva.getHabitacion().getIdHabitacion());
            pst.setInt(7, reserva.getCliente().getIdcliente());


            pst.setInt(8, reserva.getIdReserva());
            int n = pst.executeUpdate();

            if (n != 0) {
                return true;
            } else {
                return false;
            }



        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    @Override
    public boolean eliminar(int id) throws RemoteException {
        sSQL = "delete from reserva where idreserva=?";

        try {
            RMI.conexion conn = new RMI.conexion("reservaHotel", host, user, pass);
            con = conn.getLink();
            PreparedStatement pst = con.prepareStatement(sSQL);
            pst.setInt(1, id);
            int n = pst.executeUpdate();


            if (n != 0) {
                return true;
            } else {
                return false;
            }

        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    @Override
    public List<Reserva> buscarTodos() throws RemoteException {
        sSQL = "select * from reserva ";

        try {
            RMI.conexion conn = new RMI.conexion("reservaHotel", host, user, pass);
            con = conn.getLink();
            Reserva r = null;
            Statement st = con.createStatement();
            ResultSet rs = st.executeQuery(sSQL);
            List<Reserva> reservas = new ArrayList<Reserva>();
            while (rs.next()) {
                r = new Reserva();
                r.setIdReserva(rs.getInt("idreserva"));
                r.setTipo_reserva(rs.getString("tipo_reserva"));
                r.setFecha_reserva(rs.getDate("fecha_reserva"));
                r.setFecha_ingreso(rs.getDate("fecha_ingreso"));
                r.setFecha_salida(rs.getDate("fecha_salida"));
                r.setCosto(rs.getDouble("costo"));
                r.getHabitacion().setIdHabitacion(rs.getInt("idhabitacion"));
                r.getCliente().setIdcliente(rs.getInt("idcliente"));


                reservas.add(r);
            }
            return reservas;

        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public Reserva buscarReserva(int id) throws RemoteException {
        sSQL = "select * from reserva where idreserva=?";
        Reserva r = null;
        try {
            RMI.conexion conn = new RMI.conexion("reservaHotel", host, user, pass);
            con = conn.getLink();
            PreparedStatement pst = con.prepareStatement(sSQL);
            pst.setInt(1, id);

            ResultSet rs = pst.executeQuery(sSQL);


            while (rs.next()) {
                r = new Reserva();
                r.setIdReserva(rs.getInt("idreserva"));
                r.setTipo_reserva(rs.getString("tipo_reserva"));
                r.setFecha_reserva(rs.getDate("fecha_reserva"));
                r.setFecha_ingreso(rs.getDate("fecha_ingreso"));
                r.setFecha_salida(rs.getDate("fecha_salida"));
                r.setCosto(rs.getDouble("costo"));
                r.getHabitacion().setIdHabitacion(rs.getInt("idhabitacion"));
                r.getCliente().setIdcliente(rs.getInt("idcliente"));

            }
            return r;

        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}