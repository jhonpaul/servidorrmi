/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package RmiHotel;
import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.List;


public interface interfReserva extends Remote{
    public boolean guardar(Reserva reserva) throws RemoteException;
    public boolean actualizar(Reserva reserva) throws RemoteException;
    public boolean eliminar(int id) throws RemoteException;
    public List<Reserva> buscarTodos() throws RemoteException;
    public Reserva buscarReserva(int id) throws RemoteException;
    
   
    
}