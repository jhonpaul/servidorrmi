
package RmiHotel;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.List;


public interface interfHabitacion extends Remote{
    public boolean guardar(Habitacion habitacion) throws RemoteException;
    public boolean actualizar(Habitacion habitacion) throws RemoteException;
    public boolean eliminar(int id) throws RemoteException;
    public List<Habitacion> buscarTodos() throws RemoteException;
    public Habitacion buscarHabitacion(int id) throws RemoteException;
    
}